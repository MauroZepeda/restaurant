angular.module('myApp').factory('ProductoServices', ['$http', function($http) {
 
    var urlBase = '/producto';
    var dataFactory = {};

    dataFactory.getProductos = function() {
        return $http.get(urlBase + '/getProductos');
    };
 
    return dataFactory; 
}]);