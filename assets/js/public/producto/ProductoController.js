app.controller('ProductoController',  function($scope, ProductoServices) { 
 
    obtenerProductos(); 
    
    function obtenerProductos() 
    { 
    	ProductoServices.getProductos()
            .then(function (response) {
                $scope.listaProductos =  response.data;
            }, function (error) {
                console.log(error);
            });
    } 
});