var app = angular.module("myApp", ["ngRoute"]);

app.config(function($routeProvider, $locationProvider ) {
    
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
 
    $routeProvider
        .when("/", {
            templateUrl: "/views/producto",
            controller: "ProductoController"
        }) 
        .otherwise({
            templateUrl: "/error"
        });

});
