/**
 * ProductoController
 *
 * @description :: Server-side logic for managing Productoes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    listar: function(req, res) {
        Producto.find({}).exec(function(err, productos) {
            if (err) {
                return res.serverError(err);
            } 
            return res.json(productos);
        });
    }
};